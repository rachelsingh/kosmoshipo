# Kosmoshipo

![Screenshot](https://github.com/Esperanto-Arcade/Kosmoshipo/blob/master/Screenshot.png?raw=true)

Kosmoŝipo videoludeto. Mortigu la alimondulojn!

## Kiel ludi

1. [Elŝutu la ludon](https://github.com/Esperanto-Arcade/Kosmoshipo/archive/master.zip)
1. [Elŝutu PyGame](http://www.pygame.org/news.html) kaj [Python](http://www.python.org/) 
1. Eltiru la zip-dosieron al via fiksita disko.
1. Lanĉo "main.py" kun Python (Eble vi devas uzi la komandan linion)

## Informo

Kreis de [Rachel J. Morris](http://www.moosader.com/)

## License

MIT License (Vidu la "LICENSE" dosiero)

## Esperanto Arcade

Retpaĝaro: [Esperanto Arcade sur GitHub](https://github.com/Esperanto-Arcade)

Kreu malfermitkodajn videoludojn!


