# Rachel J. Morris 		2013-07-24		WTFPL CopyFree license http://copyfree.org/licenses/wtfpl2/license.txt

import pygame

class Malamiko:
	def __init__( self, bildo ):
		self.m_bildo		= bildo
		
		self.m_w = 32
		self.m_h = 32
		
		self.m_x = 640
		self.m_y = 480/2 - self.m_h/2
		
		self.m_rapido = 4
	# __init__
		
	def Movi( self, sxipo ):
		self.m_x -= self.m_rapido
		
		if ( self.m_y < sxipo.m_y ):
			self.m_y += self.m_rapido / 2
			
		elif ( self.m_y > sxipo.m_y ):
			self.m_y -= self.m_rapido / 2
	# Movi
	
	def CxuKolizio( self, sxipo ):
		return ( 	self.m_x 			< sxipo.m_x + sxipo.m_w and
					self.m_x + self.m_w > sxipo.m_x and
					self.m_y 			< sxipo.m_y + sxipo.m_h and
					self.m_y + self.m_h > sxipo.m_y )
	# CxuKolizio
	
	def CxuMalproksima( self ):
		return self.m_x < -32
	# CxuMalproksima
		
	def Vidigi( self, fenestro ):
		fenestro.blit( self.m_bildo, ( self.m_x, self.m_y ) )
	# Vidigi
# Malamiko
