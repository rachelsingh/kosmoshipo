# Rachel J. Morris 		2013-07-24		WTFPL CopyFree license http://copyfree.org/licenses/wtfpl2/license.txt

import pygame
import sys

from sxipo import Sxipo
from malamiko import Malamiko

pygame.init()
fenestro 		= pygame.display.set_mode( ( 640, 480 ) )

fontObj 		= pygame.font.Font( "fonts/Averia-Bold.ttf", 24 )
fpsTempo 		= pygame.time.Clock()

bildoSxipo 		= pygame.image.load( "graphics/Ship.png" ) 
bildoKuglo 		= pygame.image.load( "graphics/Bullet.png" ) 
bildoMalamiko	= pygame.image.load( "graphics/Alien.png" ) 

ludanto = Sxipo( bildoSxipo, bildoKuglo )

malamikoListo = []

poentaro = 0
tempo = 0
fini = False
while fini == False:
	for okazo in pygame.event.get():
		if ( okazo.type == pygame.QUIT ):
			pygame.quit()
			sys.exit()
	# for event
	
	tempo += 1
	
	if ( tempo % 50 == 0 ):
		novaMalamiko = Malamiko( bildoMalamiko )
		malamikoListo.append( novaMalamiko )
		
	forigiMalamiko = []
	for malamiko in malamikoListo:
		malamiko.Movi( ludanto )
		
		for kuglo in ludanto.PreniKuglo():
			if ( malamiko.CxuKolizio( kuglo ) ):
				forigiMalamiko.append( malamiko )
				poentaro += 1
		
		if ( malamiko.CxuKolizio( ludanto ) ):
			fini = True
	
	for malamiko in forigiMalamiko:
		malamikoListo.remove( malamiko )
	
	klavoj = pygame.key.get_pressed()
	ludanto.Movi( klavoj )
	pygame.display.update()
	fpsTempo.tick( 60 )
	
	fenestro.fill( pygame.Color( 50, 0, 100 ) )
		
	for malamiko in malamikoListo:
		malamiko.Vidigi( fenestro )
	
	ludanto.Vidigi( fenestro )
	
	poentaroTxt = fontObj.render( "Poentaro: " + str( poentaro ), False, pygame.Color( 255, 255, 255 ) )
	
	poentaroRekt = poentaroTxt.get_rect()
	poentaroRekt.x = 10
	poentaroRekt.y = 10
	
	fenestro.blit( poentaroTxt, poentaroRekt )
	
# while fini == False

# Ludo Fino
while True:
	teksto = fontObj.render( "Ludfino", False, pygame.Color( 255, 0, 0 ) )
	tekstoRekt = teksto.get_rect()
	tekstoRekt.x = 640/2 - 50
	tekstoRekt.y = 480/2 - 12
	
	pygame.display.update()
	fpsTempo.tick( 60 )
	
	fenestro.fill( pygame.Color( 0, 0, 0 ) )
	fenestro.blit( teksto, tekstoRekt )

