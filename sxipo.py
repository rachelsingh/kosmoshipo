# Rachel J. Morris 		2013-07-24		WTFPL CopyFree license http://copyfree.org/licenses/wtfpl2/license.txt

import pygame

from kuglo import Kuglo

class Sxipo:
	def __init__( self, bildo, kugloBildo ):
		self.m_bildo		= bildo
		self.m_kugloBildo 	= kugloBildo
		
		self.m_w = 48
		self.m_h = 32
		
		self.m_x = 10
		self.m_y = 480/2 - self.m_h/2
		
		self.m_rapido = 5
		
		self.m_kugloListo = []
		
		self.m_kugloTempo = 0
	# __init__
		
	def PreniKuglo( self ):
		return self.m_kugloListo
		
	def Movi( self, klavoj ):
		if ( klavoj[ pygame.K_UP ] ):
			self.m_y -= self.m_rapido
			
		elif ( klavoj[ pygame.K_DOWN ] ):
			self.m_y += self.m_rapido
			
		if ( klavoj[ pygame.K_LEFT ] ):
			self.m_x -= self.m_rapido
			
		elif ( klavoj[ pygame.K_RIGHT ] ):
			self.m_x += self.m_rapido
			
		if ( klavoj[ pygame.K_SPACE ] and
			 self.m_kugloTempo <= 0 ):
			novaKuglo = Kuglo()
			novaKuglo.Krei( self.m_x, self.m_y, self.m_kugloBildo )
			self.m_kugloListo.append( novaKuglo )
			self.m_kugloTempo = 35
			
		if ( self.m_y < 0 ):
			self.m_y = 0
		elif ( self.m_y > 480 - self.m_h ):
			self.m_y = 480 - self.m_h
			
		if ( self.m_x < 0 ):
			self.m_x = 0
		elif ( self.m_x > 640 - self.m_w ):
			self.m_x = 640 - self.m_w
	
		
		forigiListo = []
		for kuglo in self.m_kugloListo:
			kuglo.Movi()
			
			if ( kuglo.CxuMalproksima() ):
				forigiListo.append( kuglo )
		
		for kuglo in forigiListo:
			self.m_kugloListo.remove( kuglo )
			
		if ( self.m_kugloTempo > 0 ):
			self.m_kugloTempo -= 1
	# Movi
		
	def Vidigi( self, fenestro ):
		fenestro.blit( self.m_bildo, ( self.m_x, self.m_y ) )
		
		for kuglo in self.m_kugloListo:
			kuglo.Vidigi( fenestro )
	# Vidigi
# Sxipo
